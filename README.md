# Faulkner at UVA
This scrapes the old faulkner site built with Apache Cocoon and Solr, and
converts it to plain HTML/CSS and Solr in a Docker container.

To scrape the old site:
- `wget --mirror --no-host-directories --no-parent --page-requisites --convert-links --convert-file-only --adjust-extension --directory-prefix=static-content http://faulkner.lib.virginia.edu`
- Edit the 'Contexts' sections of the site.
    - The scraping process adds the full file name to links. This breaks the
      anchor tags and the javascript needed to have the images and text links
      pop up other information. For each file in the 'Contexts' section, just
      search and replace to remove the filename.  In Vim:
        - press `:` then `%s/page%3Fid=essays&amp;section=intro.html#/#/` then press Enter/Return
        - do this for each of the context pages, intro, 
- audio not working 
  - http://faulkner.lib.virginia.edu/display/wfaudio32.html#wfaudio32.13
  - need to download the files from /static/audio/clips/ to the right place
    - `scp -r sdsv2:/static/* beagle:/storage/faulkner/static-content/static/`
- build a page with link to all data in the solr index and builds the page,
  then scrape that. This replaces the static files from the original scrape.
  - modify the solrSearch.js file to return all the results when the
    search.html page is loaded (requires commenting out the 'highlight'
    variable and instances of that usage, around line 82).
  - pull up the search.html page (from the docker-ized version)
  - save the page after it returns all of the results with links to the
    individual record's page (I named this grover, just for fun).
  - upload that file/directory to the old server and modify the sitemap.xmap
    with an entry for the saved page (grover).
  - check that the grover page loads on the old production site, and the links
    to the item pages work correctly.
  - rescrape the website.
    - `wget --mirror --no-host-directories --no-parent --page-requisites --convert-links --convert-file-only --adjust-extension --directory-prefix=new-static-content http://faulkner.lib.virginia.edu/grover/grover.html`

- to more fully reflect the original, the search page redirects to a 'results'
  page which displays the results.
- The solr config was changed, the managed-schema file, so that the fulltext
  field is using 'text_en' in order to return the same results as the original.


# Development
- Download the repo
