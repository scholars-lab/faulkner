const RESULTS_PER_PAGE = 10;

//const theDomain = 'https://faulkner.internal.lib.virginia.edu';
//const theURL = theDomain + ':8989/solr/fcore/select?df=fulltext&rows=2000&';
const theDomain = 'https://faulkner.lib.virginia.edu';
const theURL = theDomain + '/solr/fcore/select?df=fulltext&rows=' + RESULTS_PER_PAGE + '&';



function xhrSuccess() { 
    this.callback.apply(this, this.arguments); 
}

function xhrError() { 
    console.error(this.statusText); 
}

function loadFile(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.callback = callback;
    xhr.arguments = Array.prototype.slice.call(arguments, 2);
    xhr.onload = xhrSuccess;
    xhr.onerror = xhrError;
    xhr.open("GET", url, true);
    xhr.send(null);
}

function showMessage(message) {
    console.log(message + this.responseText);
}

function querySolr() {
    if ( window.location.search ) {
        const query = buildQueryURL();

        loadFile(query, displayResult);
    }
}

function buildQueryURL() {

    const urlParams = new URLSearchParams(window.location.search)
    const type = urlParams.get('type');
    let fulltext = urlParams.get('fulltext');
    const q = urlParams.get('q');
    if ( null != q ) {
        fulltext = q;
    }
    const sort = urlParams.get('sort');
    theSort = '';
    if ("relevance" != sort && null != sort) {
        theSort = '&sort=' + sort + '';
    }

    let start = 0;
    if (urlParams.get('start')) {
        start = urlParams.get('start'); 
    }

    const query = `${theURL}start=${start}&hl=on&hl.fl=fulltext&fq=type:${type}&q=fulltext:"${fulltext}"${theSort}`

    return query;
}

function numResults(resultsObject){
    // Get the search form element
    const searchForm = document.getElementById("searchForm");
    // create a new span element
    const numResults = document.createElement("span");
    // set the id to numResults - for CSS styling
    numResults.setAttribute("id", "numResults");
    // plug the new numResults element into the searchForm element
    searchForm.appendChild(numResults);

    const next_number = resultsObject.response.start + resultsObject.response.docs.length;
    // Build the text and plug it into numResults element 
    // (Note: using ES6 Template Literals, ie JS magic - finally)
    if (resultsObject.response.numFound > 1) {
        numResults.innerHTML = `<b>${resultsObject.response.start + 1}</b> to <b>${next_number}</b> of <b>${resultsObject.response.numFound}</b> sorted by `;
        return;
    }

}

function regenerateSearchField(){
    const urlParams = new URLSearchParams(window.location.search);
    let type = document.getElementById("type");
    type.setAttribute("value", urlParams.get('type'));

    const fulltextInput = document.getElementById("fulltext");
    let fulltext = urlParams.get('fulltext');
    const q = urlParams.get('q');
    if ( null != q ) {
        fulltext = q;
    }
    fulltextInput.setAttribute("value", fulltext);

    const sort = urlParams.get("sort");
    if ( null == sort || 'relevance' == sort) {
        document.getElementById("relevance").selected = "true";
    } else if ( sort.includes("asc") ) {
        document.getElementById("asc").selected = "true";
    } else {
        document.getElementById("desc").selected = "true";
    }
}

function buildPagination(results_total) {
    const url = new URL(window.location.origin + window.location.pathname);
    let searchParams = new URLSearchParams(window.location.search);
    var params = (new URL(document.location)).searchParams;
    var current_start = parseInt(params.get('start')); // value of start parameter
    // if the URL parameter 'start' is empty, change it to 0, the first page 
    if (!current_start) {
        current_start = 0;
    }
    // the start param returns the results to start on, to convert to the current
    // page number, divide by ten and add 1.
    var cp = (current_start / 10) + 1; // current page number

    // If there are no results, just return some text saying that.
    if (results_total < 1) {
        const result = document.getElementById('searchResults');
        result.insertAdjacentHTML('afterbegin', '<h2>No results found.</h2>');
        return;
    }

    // Generate an array of page numbers, using the pagination function from
    // the pagination.js file
    let pages_list = pagination(results_total, current_start, RESULTS_PER_PAGE);

    const paginate = document.getElementById('pagination'); 

    // loop through the array of pages, and construct a link for each page
    // number. The current page number does not become a link. Also create the
    // previous and next links.
    for (n = 0; n < pages_list.length; n++) {
        var page_num = pages_list[n]; // the page number from the array

        // if the page number is actually an elipse, just return an elipse
        // without a link
        if (page_num == "...") {
            paginate.insertAdjacentHTML('beforeend', `<span class="page-link">${page_num}</span>`);

        } else {
            // start = where the search results should start. This is the page
            // number (in the array), minus one (1), times the number of
            // results per page
            var start = (page_num - 1) * RESULTS_PER_PAGE; 

            
            // if the page number in the array that we are on is the same as
            // the current page (calculated from the start parameter) then this is
            // the current page, 
            if (page_num == cp) {

                // Show the previous link if the current page is greater than 5
                if (cp > 5) {
                    // the start value should be the results per page quantity less than the current start value
                    params.set('start', start - RESULTS_PER_PAGE);
                    paginate.insertAdjacentHTML('afterbegin', `<span class="page-link"><a href='${url.toString()}?${params.toString()}'><< Prev</a></span>`)
                }

                // Show the next link if the current page is 5 places less than
                // the last one (which you get by getting the value of the last
                // element in the pages_list array) then subtract 5
                if (cp < pages_list[pages_list.length - 1] - 4 ) {
                    // if the current page is 1, then start should be 0
                    if (cp == 1){
                        start = 0;
                    }
                    params.set('start', start + RESULTS_PER_PAGE);
                    paginate.insertAdjacentHTML('afterend', `<div class="page-link next-link"><a href='${url}?${params.toString()}'>Next >></a></div>`)
                }

                // Show the current page number, without a link, then skip the
                // rest of the code in the loop and go to the next element in
                // the array
                paginate.insertAdjacentHTML('beforeend', `<span class="page-link current-page-link">${page_num}</span>`);
                continue;
            }

            // display the rest of the links
            params.set('start', start);
            paginate.insertAdjacentHTML('beforeend', `<span class="page-link"><a href='${url}?${params.toString()}'>${page_num}</a></span>`);
        }


    }

}



/** Displays the results from solr.
  * Manipulates the search.html page, adding a div to display the number of
  * results, and a list of each item from the search results.
  *
  * @param {object} searchRequest - The JSON object returned by the solr database; the search results.
*/
function displayResult() {
    // Get all the results back from Solr
    const jObj = JSON.parse(this.response);

    // plug in search term, and set the drop down to correct value
    regenerateSearchField();

    // generate the text to display the number of items found
    numResults(jObj);


    // Get the element where we plug in all of the results
    const searchResults = document.getElementById("searchResults");

    const resultsArray = Object.entries(jObj.response.docs);

    
    buildPagination(jObj.response.numFound);

    // Put the results object into an array. Loop through each element of the
    // array. Using for loop instead of foreach for better performance
    for ( const item of resultsArray ){
        // create a new div element
        let e = document.createElement("div");

        // Set the class of the element (for zebra striping the results)
        let theClass = "result_div_odd";
        if ( (item[0] +1)%2 == 0 ){
            theClass = "result_div_even";
        }
        e.className = theClass;

        
        // pull the text with the search term highlighted from the object
        // directly since it's not included in the array info
        let highlight = jObj.highlighting[item[1].id].fulltext;

        // if it's a transcription, create the html for it
        if ( item[1].type == "transcription" ) {
            htmlStuff = `
            <dl>
                <a href="display/${item[1].doc_id}#${item[1].id}">${item[1].head}</a>
                <div class="player-line">
                <span>
                    <audio id="${item[1].id}" controls> 
                    <source src="${theDomain}/static/audio/clips/${item[1].id}.ogg" type="audio/ogg">
                    <source src="${theDomain}/static/audio/clips/${item[1].id}.mp3" type="audio/mp3">
                    <p>Your browser doesn't support HTML5 audio. Here is a <a href="${theDomain}/static/audio/clips/${item[1].id}.mp3">link to the audio</a> instead.</p>
                    </audio>
                </span>
                </div><!-- player-line -->
                <div>
                <dt>Date:</dt>
                <dd>${item[1].date_display}</dd>
                </div>
                <div> 
                <dt>Participants:</dt>
                <dd>${item[1].location}</dd>
                </div>
                <div>
                <dt>Snippet:</dt>
                <dd>${highlight}</dd>
                </div>
            </dl>`;

        //if it's a prose, generate the html for it
        } else if ( item[1].type == "prose" ){
            let image = '';
            let padding = '';
            if ( item[1].thumb_image ) {
               image = `
                 <div class="search-image-div">
                   <a href="${theDomain}/media?id=${item[1].id}"><img src="${theDomain}/${item[1].thumb_image}" class="search-image"></a>
                 </div>`;

               padding = ' class="link-padding"'
            }

            htmlStuff = `
            <dl>
                ${image}
                <a href="${theDomain}/media?id=${item[1].id}">${item[1].section_title}</a>
                <div ${padding}>
                    <dt>Snippet:</dt>
                    <dd>${highlight}</dd>
                </div>
            </dl>`;
        }

        e.insertAdjacentHTML("afterbegin", htmlStuff);
        searchResults.appendChild(e);
    };
}



// One time use function to create a temporary page (grover/grover.html) that
// lists all of the records from the solr database. The list is then saved to
// the production site so that each of the individual bibliography pages are
// linked to, which will then allow the scrape to generate a static file for
// each bib. page.
function allFromSolr() {
  let query = theURL + "q=*:*";
  if (request) {
    request.open('GET', query, true);
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
          let jObj = JSON.parse(request.response);
          let resultDiv = document.getElementById("allResults");

          let numResults = document.createElement("div");
          resultDiv.appendChild(numResults);
          numResults.innerHTML = "Results Found: " + jObj.response.numFound;

          let resultList = document.createElement("ul");
          resultDiv.appendChild(resultList);
          let theClass = "result_odd";
          jObj.response.docs.forEach(function(item, index){
            if ( (index+1)%2 == 0 ){
              theClass = "result_even";
            } else {
              theClass = "result_odd";
            }

            let text = item.id + ". " + item.title;
            resultList.innerHTML += "<li class='" + theClass + "'>" + "<a href='" + theDomain + "/media%3fid=" + item.id.trim() + "'>" + text + "</a></li>";
          });

      }
    }
    request.send();
  } else {
    document.getElementById("searchError").textContent = "There was a problem connecting to solr.";
  }
}

