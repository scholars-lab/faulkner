$(function() {
  $('.icon-play-sign').click(function() {

    $(this).hide('slow');

//    var start = $(this).attr('data-start');
//    var end = $(this).attr('data-end');
    
    var audio = new Audio();
    var canPlayOgg = !!audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"') !== "";
//    var file = $('.entireclip > audio').attr('id');
    var file = $(this).attr('data-clip');

    var baseUrl = "https://faulkner.lib.virginia.edu/static/audio/clips/";
    baseUrl += canPlayOgg ? file + ".ogg" : file + '.mp3';
//    baseUrl += "#t=" + start + "," + end;
    console.log('url', baseUrl);
    audio.setAttribute('src', baseUrl);
    audio.controls = true;
    audio.preload = 'auto';

    $(this).parent().append(audio).show('slow');
    audio.play();

  });
});
