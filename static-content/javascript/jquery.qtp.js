
/*
 * Audio player pop-up modals for essays.
 */

$(function() {

  $('a.inline').click(function(e) {

    e.preventDefault();
    var modal = $($(this).attr('href'));

    modal.dialog({

      minWidth: 640 ,

      open: function() {

        // Get filename, clip boundaries.
        //var file  = $(this).attr('data-file');
        var file  = $(this).attr('id');
        var start = $(this).attr('data-start');
        var end   = $(this).attr('data-end');

        // Chop off hundreths of seconds.
        start = start.substring(0, start.length-3);
        end = end.substring(0, end.length-3);

        var audio = new Audio();
        var ext = (!!audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"') !== "") ? '.ogg' : '.mp3';
        var url = 'http://faulkner.lib.virginia.edu/static/audio/clips/' + file + ext;
        //var url = 'http://faulkner.lib.virginia.edu/static/audio/clips/' + file + ext + '#t=' + start + ',' + end;

        console.log('url', url);
        audio.setAttribute('src', url);
        audio.controls = true;
        audio.preload = 'auto';

        $(this).append(audio);
        audio.play();

      },

      close: function() {
        $(this).find('audio').remove();
      }

    });

  });

});
