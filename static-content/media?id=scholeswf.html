<!DOCTYPE html SYSTEM "about:legacy-compat">
<html>
<head>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Faulkner at Virginia:
					Media</title>
<link type="text/css" href="/style.css" rel="stylesheet" media="screen,projection">
<link type="text/css" href="/print.css" rel="stylesheet" media="print">
        <!--[if lt IE 9]>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
        <![endif]--></head>
<body>
        <!--[if lt IE 9]>
         <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
         <![endif]-->
       <div id="wrap">
<div id="header">
<div class="banner">
<img src="/header.jpg" alt="Faulkner at Virginia: An Audio Archive"></div>
<div id="main_nav">
<ul class="menu">
<li>
<a href="./index.html">Home</a>
</li>
<li>
<a href="page%3Fid=essays&amp;section=intro.html">Contexts</a>
</li>
<li>
<a href="browse.html">Browse</a>
</li>
<li>
<a href="search.html">Search</a>
</li>
<li>
<a href="page%3Fid=clips&amp;section=selections.html">Clips</a>
</li>
<li>
<a href="page%3Fid=about&amp;section=credits.html">About</a>
</li>
</ul>
</div>
</div>
<div class="content">
<a xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="scholeswf"></a>
<h3 class="center">Mr. Faulkner in My Classroomr</h3>
<h3 class="center">By Robert Scholes</h3>
<blockquote>[<em>This essay was first published, in a slightly longer form, in </em>The Brown Alumni Magazine<em> (May/June 2006), and is
					reprinted here with the generous permission of the </em>BAM<em>. Professor Scholes taught at UVA between 1959 and 1964.
					Faulkner&rsquo;s visit to his class probably occurred in 1962, during one of the returns to UVA that the novelist made every
					spring between the end of his second residency and his death.</em>]</blockquote>
<p>The main event in this narrative is a visit paid by William Faulkner to my undergraduate honors seminar in the novel, on a day when we
				were discussing his novel <em>Absalom, Absalom!</em> I had occasion recently to mention this episode in the preface to a book of mine,
				and I have been getting questions about it ever since. People want to know what this experience was like, what Mr. Faulkner was like,
				how he responded to students &ndash; and things of that sort. I will try to answer these questions, but first I need to provide a
				bit of background on that moment in our history as it played out in and around the university that Thomas Jefferson designed and
				founded many years before.</p>
<p>I started teaching at the University of Virginia in 1959, fresh out of graduate school at Cornell, where I had contemplated writing a
				dissertation on Joseph Conrad and William Faulkner, but had dropped that idea when offered the newly acquired papers of James Joyce to
				catalogue instead. But Faulkner was a star in my firmament before I ever got to shake his hand or chat with him at a party. He had
				been a Writer-in-Residence at UVA for some years before I got there, spending half the year there, and dutifully allowing
				conversations with faculty to be recorded and published. I had read a volume of these before I even got there. But these were not
				quiet times in that part of the world.</p>
<p>I remember being interviewed in a hotel room at the meeting of the Modern Language Association by a disgruntled professor who waxed
				eloquent on the subject of what was called &ldquo;massive resistance&rdquo; &ndash; the closing of the public schools in
				Charlottesville, rather than integrate them as the law had begun to require. The professor left the University of Virginia and I
				arrived, after being assured that the schools were indeed reopening, so that my two little ones could attend them. But other things
				were going on. There were no female students at Virginia in those days, and, at football games, black people sat in the end zone and
				whites along the side of the field. The local movie theatre made all black people sit in the balcony, and I soon got into a
				controversy in the newspaper with someone who insisted that this was necessary because they smelled bad. I remember responding that
				some white people smelled worse than some black people, and suggesting that a smellometer be put in the lobby and everyone with a high
				score be sent upstairs &ndash; but this did not happen, as you might expect.</p>
<p>As a Yankee, born in Brooklyn, educated in New York and Connecticut, I was really in no position to pontificate about all this to the
				locals. But I had white southern colleagues who played a leading role in the struggle over civil rights, sitting in with blacks at
				local diners and generally proving that not all southerners were bigoted or cowardly. The position of Faulkner in all this was
				interesting. Any serious reader of his novels knew that he did not think white folks were necessarily smarter, more sensitive, or
				morally superior to blacks. I remember asking him at a conference about his book, <em>Go Down, Moses</em>, which he always called a
				novel, though it looked to many people like a collection of stories about different people over a long period of time. They had
				stopped tape-recording his conferences by this time, and I think he was a bit more forthcoming because of that. In any case, he
				explained patiently to me that the book was unified by taking place entirely on the same bit of territory, and by having as its
				protagonist the Negro race as a whole. As the title implied, it was about their evolution toward freedom and equality.</p>
<p>I should point out that Faulkner took his obligations as Writer-in-Residence seriously, interpreting them as a responsibility toward
				the students in particular. During this struggle over civil rights he spoke at least once to a group of undergraduates about
				integration. What I learned about that meeting suggests that he told them that integration was right and should happen, but that he
				thought it needed some time and shouldn&rsquo;t be hurried. As we know now, it came pretty fast, but with many gaps and
				difficulties that helped keep racism alive and well, with politicians in the south and elsewhere playing the race card for years
				afterward with varying degrees of subtlety. I don&rsquo;t want to suggest that it should have come more slowly, only to point out
				that Faulkner was right to be concerned about the pace of the process and how that might affect the results.</p>
<p>It was his concern for the students, and his commitment to them, however, that brought him to my seminar. As I recall, this visit was
				arranged by Joe Blotner, a colleague who had generally squired Mr. Faulkner around the university, tape-recorded the interviews, and
				ultimately, some years after his death, published a major biography of Faulkner. Mr. Faulkner, as we called him, liked Joe and was at
				ease with him and his family. Virginia was a rather formal place in those days. We referred to the founder as Mr. Jefferson, and my
				department chair told his faculty that we should dress as well as the students. (I remember asking him if he meant qualitatively or
				quantitatively, and he said he meant we should wear coats and ties. This gentleman was also my landlord, and when he promoted me from
				Instructor to Assistant Professor, he actually raised my rent.)</p>
<p>On the big day, Mr. Faulkner came to Joe&rsquo;s office, as I recall, and I met him there and walked him down the hall to the
				seminar room, where he joined us around a large table. He had re-read the book, he said, so he could respond to questions about it.
				So, the students started in and he was as forthcoming and serious as anyone might wish. At social events, I should point out, this was
				notoriously not the case. People trying to talk literature with him over a class of bourbon, were rebuffed, usually politely. I
				remember talking to him about cock-fighting at such gatherings and things like that &ndash; about which I knew nothing, but I was
				trying to learn as much as I could about southern culture, and what better teacher could one have? But he was not the same reticent
				country gentleman in the seminar room with students.</p>
<p>If I were the sort of memoirist who gets on Oprah, I would probably remember every detail of that class, with advantages, but it is all
				a blur in my memory, except that I recall my own major question, which I saved until the students had satisfied to some extent their
				own thirst for knowledge. In reading <em>Absalom, Absalom!</em> we learn about the past first from those closest to the events and
				gradually move further away until at the end, what we are getting is the imaginative reconstruction of that past by two college
				students who have a large emotional investment in what happened and what it meant. I told Mr. Faulkner that it seemed to me that the
				further away we got from the eye-witnesses and the deeper we got into the imaginative reconstruction of events by those who were never
				near them, the closer we were coming to the truth. All I remember is that he confirmed my view, or seemed to. And it is a literary
				view, the view of an artist not a country gentleman. Mr. Faulkner, as it happened could play both of those roles, perfectly, and I am
				grateful that I had a chance to be an eye-witness to them, though I remain cautious about claiming that my memory speaks the truth
				about him One thing I do know. The books are great books, and they are still very much alive. And Mr. Faulkner was a great writer
				&ndash; one of the greatest we have ever had. It was a privilege to chat with him socially and a thrill to hear him talk seriously
				about literature as an art. Being an English teacher has its moments, after all.</p>
<br>
<p>&copy;2006, 2010 Robert Scholes</p>
</div>
<div id="footer">
<img src="/footer.gif" width="84" height="54" class="float_left"><div class="f_wrap">
<p>
<em>Faulkner at Virginia,</em> &copy; Rector and Visitors of the University of Virginia; Author Stephen Railton.</p>
<p>
<a href="page%3Fid=conditions&amp;section=use.html">Conditions of Use and Copyright Information</a>
</p>
</div>
</div>
</div>
<!-- Matomo --> <script> var _paq = window._paq = window._paq || []; /* tracker methods like "setCustomDimension" should be called before "trackPageView" */ _paq.push(['trackPageView']); _paq.push(['enableLinkTracking']); (function() { var u="https://analytics.lib.virginia.edu/"; _paq.push(['setTrackerUrl', u+'matomo.php']); _paq.push(['setSiteId', '45']); var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s); })(); </script> <!-- End Matomo Code --></body>
</html>
