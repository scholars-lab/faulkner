

//const query = 'http://faulkner.lib.virginia.edu:8899/solr/fcore/select?df=fulltext&rows=2000&fq=type:transcription&q=fulltext:lawn'
const theURL = 'http://faulkner.lib.virginia.edu:8899/solr/fcore/select?df=fulltext&rows=2000&'


function xhrSuccess() { 
    this.callback.apply(this, this.arguments); 
}

function xhrError() { 
    console.error(this.statusText); 
}

function loadFile(url, callback /*, opt_arg1, opt_arg2, ... */) {
    console.log(url);
    const xhr = new XMLHttpRequest();
    xhr.callback = callback;
    xhr.arguments = Array.prototype.slice.call(arguments, 2);
    xhr.onload = xhrSuccess;
    xhr.onerror = xhrError;
    xhr.open("GET", url, true);
    xhr.send(null);
}

function showMessage(message) {
    console.log(message + this.responseText);
}

function querySolr() {
    if ( window.location.search ) {
        document.getElementById('searchSection').remove();
        const query = buildQueryURL();

        loadFile(query, displayResult);
    }
    if ( window.onload ) {
        const query = theURL + 'fq=type:prose&q=*:*';
        document.getElementById('searchSection').remove();
        console.log(query);
        loadFile(query, displayResult);

    }
}

function buildQueryURL() {

    const urlParams = new URLSearchParams(window.location.search)
    const type = urlParams.get('type');
    const fulltext = urlParams.get('fulltext');

    const query = theURL + 'hl=on&hl.fl=fulltext&fq=type:' + type + '&q=fulltext:"' + fulltext + '"'

    return query;
}

/** Displays the results from solr.
  * Manipulates the search.html page, adding a div to display the number of
  * results, and a list of each item from the search results.
  *
  * @param {object} searchRequest - The JSON object returned by the solr database; the search results.
*/
function displayResult() {
    var jObj = JSON.parse(this.response);
    var resultDiv = document.getElementById("searchResult");

    var numResults = document.createElement("div");
    resultDiv.appendChild(numResults);
    numResults.innerHTML = "Results Found: " + jObj.response.numFound;

    var resultList = document.createElement("ul");
    resultDiv.appendChild(resultList);
    var theClass = "result_odd";
    jObj.response.docs.forEach(function(item, index){
        if ( (index+1)%2 == 0 ){
            theClass = "result_div_even";
        } else {
            theClass = "result_div_odd";
        }

        //var highlight = jObj.highlighting[item.id].fulltext;
        if ( item.type == "transcription" ) {
            htmlStuff = 
            '<div class=' + theClass + '>' +
                '<dl>' +
                    '<a href="display/' + item.doc_id + '#' + item.id +'">' + item.head + '</a>' +
                    '<div class="player-line">' +
                        '<span><i class="icon-play-sign icon-2x" id="' + item.tape_id + '" data-clip="' + item.id + '" data-start="' + item.start + '" data-end="' + item.end + '"></i></span>' +
                    '</div>' +
                    '<div>' + 
                        '<dt>Date:</dt>' + 
                        '<dd>' + item.date_display + '</dd>' + 
                    '</div>' +
                    '<div>' + 
                        '<dt>Participants:</dt>' +
                        '<dd>' + item.location + '</dd>' +
                    '</div>' +
                    '<div>' +
                        '<dt>Snippet:</dt>' + 
                        '<dd>' + highlight + '</dd>' + 
                    '</div>' + 
                '</dl>'
            '</div>';
        } else if ( item.type == "prose" ){
            var image = '';
            var padding = '';
            if ( item.thumb_image ) {
               image = '<div style="float:left;width:120px;">' +
                    '<a href="media?id=' + item.id + '"><img src="' + item.thumb_image + '" style="max-width:120px;"></a>' +
                '</div>' 
               padding =  ' style="margin-left:140px;"'
            }
            htmlStuff = 
            '<div class="' + theClass + '">' +
                '<dl>' +
                image +
                    '<div' + padding +'>' +
                        '<a href="media?id=' + item.id + '">' + item.section_title + '</a>' +
                    '</div>' +
                    '<div' + padding +'>' +
                        '<dt>Snippet:</dt>' +
                        //'<dd>: ' + highlight + '</dd>' +
                    '</div>' +
                '</dl>' +
            '</div>';
        }

        resultList.innerHTML += htmlStuff;
    });
}



// One time use function to create a temporary page (grover/grover.html) that
// lists all of the records from the solr database. The list is then saved to
// the production site so that each of the individual bibliography pages are
// linked to, which will then allow the scrape to generate a static file for
// each bib. page.
function allFromSolr() {
  var query = theURL + "q=*:*";
  if (request) {
    request.open('GET', query, true);
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
          var jObj = JSON.parse(request.response);
          var resultDiv = document.getElementById("allResults");

          var numResults = document.createElement("div");
          resultDiv.appendChild(numResults);
          numResults.innerHTML = "Results Found: " + jObj.response.numFound;

          var resultList = document.createElement("ul");
          resultDiv.appendChild(resultList);
          var theClass = "result_odd";
          jObj.response.docs.forEach(function(item, index){
            if ( (index+1)%2 == 0 ){
              theClass = "result_even";
            } else {
              theClass = "result_odd";
            }

            var text = item.id + ". " + item.title;
            resultList.innerHTML += "<li class='" + theClass + "'>" + "<a href='http://faulkner.lib.virginia.edu/media%3fid=" + item.id.trim() + "'>" + text + "</a></li>";
          });

      }
    }
    request.send();
  } else {
    document.getElementById("searchError").textContent = "There was a problem connecting to solr.";
  }
}

